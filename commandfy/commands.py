from abc import abstractmethod
from contextlib import AbstractContextManager
import copy
import sys
import logging
from typing import Union


logger = logging.getLogger(__name__)


class CommandRollbackException(Exception):
    """
    Exception class to help distinguish case when exception is raised from
    within rollback
    """
    pass


class AbstractCommand(AbstractContextManager):
    """
    Note:
        For individual commands, self.commit() has to be called,
        otherwise command will be rollbacked at __exit__

        All commands should take care of its own exceptions and set self.result to True or False
        Exception will be raised from command only in the case where self.result is None, i.e.
        unhandled, unknown exception cases, see __exit__.
    """

    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.committed = False
        self.result = None

    def __exit__(self, exc_type, exc_value, traceback):
        """
        return True in __exit__ in case where there is an exception
        will signal that the exception is handled internally
        """

        # if individual commands are not committed, then they are to be rollbacked
        if self.result and not self.committed:
            # TODO: handle rollback exceptions
            self.rollback()

        """
        Case where either command's execute() fails to return
        Re-raise the exception
        """
        if self.result is None and exc_type:

            # for individual commands, unknown exception is raised immediately after rollback
            logger.error(
                "{} ({}) raised in command but not handled".format(exc_type.__name__, sys._getframe(1).f_code.co_name)
            )

            raise

    def commit(self):
        self.committed = True

    def execute(self, *args, **kwargs):
        if self.result is not None:
            logger.error(
                'Command {} was tried to be executed more than once!'.format(type(self).__name__)
            )
            return False

        self.result = self._execute(*args, **kwargs)
        return self.result

    def rollback(self, *args, **kwargs):

        try:
            self._rollback(*args, **kwargs)

            logger.debug(
                "{} ({}) rollbacked".format(type(self).__name__, id(self))
            )

        except Exception as e:

            logger.fatal(
                "{} ({}) rollback fail".format(type(self).__name__, id(self))
            )

            raise CommandRollbackException from e

    @abstractmethod
    def _execute(self, *args, **kwargs) -> Union[bool, dict]:  # pragma: no cover
        """
        _execute needs to return either a boolean (True for success, False for failure)
        or a dict containing the key, value pairs to update the state when used in a CompositeCommand
        """
        pass

    @abstractmethod
    def _rollback(self):  # pragma: no cover
        pass


class CompositeCommand(AbstractCommand):
    """
    Utility class which executes several steps in sequence, gathering
    results into a common dictionary which subsequent steps can access.

    See `test_commands.py` for example usage.
    """
    def __init__(self, commands_to_exec=[], initial_state={}, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if type(commands_to_exec) not in (list, tuple):
            raise TypeError("commands must be a list of Commands")

        for cmd in commands_to_exec:
            try:
                if not isinstance(cmd, AbstractCommand) and not issubclass(cmd, AbstractCommand):
                    raise TypeError("elements must be subclass or instance of AbstractCommand")
            except TypeError as e:
                raise TypeError("elements must be subclass or instance of AbstractCommand") from e

        self.commands_to_exec = commands_to_exec

        self.executed_commands = []
        self._state_history = [copy.deepcopy(initial_state)]

    def __exit__(self, exc_type, exc_value, traceback):

        if self.result is None and exc_type:

            if exc_type != CommandRollbackException:
                # only rollback when the exception is not due to rollback
                self.rollback()

            logger.error(
                "CompositeCommand ({}): {} raised in composite command but not handled"
                .format(id(self), exc_type.__name__)
            )

            raise

    @property
    def state(self):
        return self._state_history[-1]

    @state.setter
    def state(self, new_state):
        self._state_history.append(new_state)

    def _execute(self, *args, **kwargs):

        for command in self.commands_to_exec:
            if type(command) == CompositeCommand:
                with command as cmd:
                    cmd_result = cmd.execute()
            else:
                command = command(**self.state)

                # single command are run outside of context manager
                # because the rollback is being manually handled by the composite command
                cmd_result = command.execute()

            self.executed_commands.append(command)

            if not cmd_result:
                """
                TODO: logs a helper warning to user that command needs to return something TRUE-y
                otherwise the command will always fail
                """
                logger.warning(
                    "CompositeCommand ({}): {} ({}) failed".format(id(self), command.__class__.__name__, id(command))
                )
                self.rollback()
                return False

            else:
                self._update_state(cmd_result)

            logger.debug(
                "CompositeCommand ({}): {} ({}) succeeded".format(id(self), command.__class__.__name__, id(command))
            )

            command.commit()

        self.commit()
        return True

    def _rollback(self):
        """Rolls back steps executed so far in reverse order of execution"""
        for command in self.executed_commands[::-1]:
            if command.committed:
                command.rollback()
                logger.debug(
                    "CompositeCommand ({}): {} ({}) rollbacked"
                    .format(id(self), command.__class__.__name__, id(command))
                )

    def _update_state(self, cmd_result):
        """
        Updates the state property with results from executed steps
        And keeps a history of the state at each step
        """
        new_state = copy.deepcopy(self.state)

        if type(cmd_result) == dict:
            new_state.update(cmd_result)

        self.state = new_state
