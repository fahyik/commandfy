# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

requirements = [
    # TODO: write script to read from requirements folder
]


setup(name='pika-fy',
      version='0.0.0',
      url='https://github.com/fahyik/pika-fy',
      license='',
      author='fahyik',
      author_email='fahyik@gmail.com',
      description='',
      packages=find_packages(exclude=['tests']),
      install_requires=requirements,
      long_description=open('README.md').read(),
      zip_safe=False,)
