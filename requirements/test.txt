# Test dependencies go here
-r base.txt

mock==2.0.0

# pytest
pytest-sugar==0.9.1
pytest-env==0.6.2
pytest-cov==2.5.1