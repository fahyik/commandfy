from datetime import datetime
import mock
import pytest

from commandfy.commands import AbstractCommand, CompositeCommand, CommandRollbackException


class CustomException(Exception):
    pass


class BaseTestCommand(AbstractCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.date_executed = None
        self.date_rolled_back = None

    def _execute(self):
        self.date_executed = datetime.now()
        return True

    def _rollback(self):
        self.date_rolled_back = datetime.now()


class SuccessCommand(BaseTestCommand):

    def _execute(self):
        """
        Adds id as key to COMMAND_RESULT
        """
        self.kwargs[id(self)] = 1
        return super()._execute()

    def _rollback(self):
        """
        Checks that command is rollbacked by checking that id exists in COMMAND_RESULT
        """
        super()._rollback()
        assert id(self) in self.kwargs.keys()


class FailureCommand(BaseTestCommand):

    def _execute(self):
        super()._execute()
        return False


class FailureRollbackCommand(BaseTestCommand):

    def _rollback(self):
        raise CustomException()


class UnhandledExceptionCommand(BaseTestCommand):

    def _execute(self):
        raise CustomException("Unhandled exception")


class CreateCommand(BaseTestCommand):
    """Example first subcommand for CompositeCommand.

    Note the kwarg `create_input` in the constructor, which
    must be supplied via `initial_state` for the CompositeCommand
    to succeed.
    """
    def __init__(self, create_input, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.create_input = create_input

    def _execute(self):
        super()._execute()
        return {'created': 'object created using {}'.format(self.create_input)}


class TestCompositeCommand():

    def test_init_validation_command_list(self):

        # first argument needs to be a list of commands
        with pytest.raises(TypeError):
            CompositeCommand(SuccessCommand, {})

        with pytest.raises(TypeError):
            CompositeCommand([SuccessCommand, object], {})

        with pytest.raises(TypeError):
            CompositeCommand([SuccessCommand, 1], {})

    def test_state_history(self):
        """
        At initialisation, composite_command's state history
        """

        initial_state = {
            "create_input": "random"
        }

        with CompositeCommand(
            [SuccessCommand, CreateCommand],
            initial_state
        ) as composite_command:

            assert initial_state == composite_command.state
            assert composite_command.execute() is True

        assert len(composite_command._state_history) == 3  # there should be 3 states, 1 initial, 2 updated

        assert len(composite_command._state_history[1].keys()) == 1
        assert 'create_input' in composite_command._state_history[1].keys()

        assert len(composite_command._state_history[2].keys()) == 2
        assert 'created' in composite_command._state_history[2].keys()
        assert 'create_input' in composite_command._state_history[2].keys()

    def test_rollback_order(self):
        """
        Given two successful steps (A,B) followed by one failing step (C),
        steps should be rollbacked in the order (B, A).
        """
        composite_command = CompositeCommand(
            [BaseTestCommand, BaseTestCommand, FailureCommand], {})
        composite_result = composite_command.execute()

        assert not composite_result
        a = composite_command.executed_commands[0]
        b = composite_command.executed_commands[1]
        assert b.date_rolled_back < a.date_rolled_back

    def test_unhandled_exception_is_only_raised_after_rollback(self):
        """
        Given two successful steps (A,B) followed by one step which raises unhandled exception(C),
        steps should be rollbacked in the order (B, A), and CustomException is raised at the end
        """

        with pytest.raises(CustomException):

            with CompositeCommand(
                [SuccessCommand, SuccessCommand, UnhandledExceptionCommand],
                {}
            ) as composite_command:

                composite_command.execute()

    def test_composite_command_rollback_failure(self):
        composite_command = CompositeCommand([FailureRollbackCommand, FailureCommand], {})

        with pytest.raises(CommandRollbackException):
            with composite_command as cmd:
                assert cmd.execute() is False

    def test_no_rollback_for_failed_first_step(self):
        """
        If first step fails, composite_result should be empty
        First step should also not be rolled back
        """
        composite_command = CompositeCommand(
            [FailureCommand, BaseTestCommand, BaseTestCommand], {})
        composite_result = composite_command.execute()

        assert not composite_result

        first_step = composite_command.executed_commands[0]
        assert first_step.date_rolled_back is None

    @mock.patch.object(SuccessCommand, 'rollback')
    def test_composite_commands_of_composites_1(self, mock_rollback):
        """
        Given a composite command consisting of two composite commands of steps (A, B) and (C, D)
        If unhandled exception is raised in B, InternalServerError should be raised and A should be rolled back
        """

        with pytest.raises(CustomException):
            composite_command = CompositeCommand(
                [
                    CompositeCommand([SuccessCommand, UnhandledExceptionCommand], {}),
                    CompositeCommand([SuccessCommand, SuccessCommand], {})
                ],
                {}
            )

            with composite_command as cmd:
                cmd.execute()

        mock_rollback.assert_called_once()

    @mock.patch.object(SuccessCommand, 'rollback')
    def test_composite_commands_of_composites_2(self, mock_rollback):
        """
        Given a composite command consisting of two composite commands of steps (A, B) and (C, D)
        If unhandled exception is raised in D, CustomException should be raised and A, B, C should be rolled back
        """

        with pytest.raises(CustomException):
            composite_command = CompositeCommand(
                [
                    CompositeCommand([SuccessCommand, SuccessCommand], {}),
                    CompositeCommand([SuccessCommand, UnhandledExceptionCommand], {})
                ],
                {}
            )

            with composite_command as cmd:
                cmd.execute()

        # there should be a total of 3 rollbacks
        assert mock_rollback.call_count == 3

    @mock.patch.object(SuccessCommand, 'rollback')
    def test_composite_commands_of_composites_3(self, mock_rollback):
        """
        Given a composite command consisting of two composite commands of steps (A, B) and (C, D)
        If command D fails, A, B, C should be rolled back
        """
        composite_command = CompositeCommand(
            [
                CompositeCommand([SuccessCommand, SuccessCommand], {}),
                CompositeCommand([SuccessCommand, FailureCommand], {})
            ],
            {}
        )

        with composite_command as cmd:
            assert cmd.execute() is False

        # there should be a total of 3 rollbacks
        assert mock_rollback.call_count == 3


class TestSingleCommand():

    def test_double_execute_is_illegal(self):
        command = BaseTestCommand()
        result = command.execute()
        assert result is True

        result = command.execute()
        assert result is False

    @mock.patch.object(BaseTestCommand, 'rollback')
    def test_uncommitted_command_is_auto_rollbacked(self, rollback_mock):

        with BaseTestCommand() as command:
            command.execute()  # returns True

        rollback_mock.assert_called_once()

    @mock.patch.object(BaseTestCommand, 'rollback')
    def test_committed_command_is_not_rollbacked(self, rollback_mock):

        with BaseTestCommand() as command:
            command.execute()
            command.commit()

        rollback_mock.assert_not_called()

    def test_single_command_exception(self):

        with pytest.raises(CustomException):
            with UnhandledExceptionCommand() as cmd:
                cmd.execute()

    def test_single_command_execute_exception(self):

        with pytest.raises(CustomException):
            with UnhandledExceptionCommand() as cmd:
                cmd.execute()

    def test_single_command_rollback_exception(self):

        with pytest.raises(CommandRollbackException):
            with FailureRollbackCommand() as cmd:
                cmd.execute()
